import { Injectable } from '@angular/core';
import { ActivityTableComponent } from './admin/activity-table/activity-table.component';


@Injectable({
    providedIn: 'root'
})
export class NavServiceService {
    visibleActivity: boolean;
    visible: boolean;
    isLogin: boolean;
    displayedcolums;
    id: number;
    activityId: string;
    name: string;
    location: string;
    date: string;
    time_start: string;
    time_end: string;
    teacher: string;
    period_reg: string;
    description: string;
    activityTable: ActivityTableComponent;
    studentId: string;
    surname: string;
    image: string;
    birth: string;
    username: string;

    setActivitys(id: number, activityId: string, name: string, location: string,
      date: string, time_start: string, time_end: string, teacher: string, period_reg: string,
      description: string) {
        this.id = id;
        this.activityId = activityId;
        this.name = name;
        this.location = location;
        this.date = date;
        this.time_start = time_start;
        this.time_end = time_end;
        this.teacher = teacher;
        this.period_reg = period_reg;
        this.description = description;

    }
    setStudent(id: number, studentId: string, name: string, surname: string,
      image: string, birth: string, username: string) {
        this.id = id;
        this.studentId = studentId;
        this.name = name;
        this.surname = surname;
        this.image = image;
        this.birth = birth;
        this.username = username;
    }
    constructor() {
        this.visible = false;
        this.visibleActivity = false;
        this.isLogin = false;
    }

    show() {
        this.visible = true;
    }

    toggle() {
        this.visible = true;
    }

    Login() {
        this.isLogin = true;
    }

    showActivity() {
        this.visibleActivity = true;
    }
    Logout() {
      this.isLogin = false;
    }
}
