import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Teacher } from '../entity/teacher';
import { TeacherService } from './teacher-service';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class TeacherDataImplService extends TeacherService {
  // constructor() {
  constructor(private http: HttpClient) {
    super();
  }
  teacher: Teacher[] = [
    {
      'id': 11,
      'teacherId': 'T-01',
      'name': 'Prasert',
      'surname': 'Somboon',
      'subject': 'Activity1',
      'image': 'https://f.ptcdn.info/190/012/000/1384583145-5568755500-o.jpg',
      'username': 'prasert@cmu.ac.th',
      'password': '12345',
      'role': 'Teacher'
    },
    {
      'id': 12,
      'teacherId': 'T-02',
      'name': 'Somjui',
      'surname': 'Roongroj',
      'subject': 'Camping',
      'image': 'https://2.bp.blogspot.com/-1HFkHcKiV_U/WoVh1IztWqI/AAAAAAAAweQ/N30ccrEVeAEZ-OcLaNevfsTeEWp2mvRjgCLcBGAs/s1600/face01.jpeg',
      'username': 'somjui@cmu.ac.th',
      'password': 'aaaa',
      'role': 'Teacher'
    },
    {
      'id': 13,
      'teacherId': 'T-03',
      'name': 'Nacy',
      'surname': 'Rambo',
      'subject': 'Activity2',
      'image': 'https://png.pngtree.com/element_origin_min_pic/16/06/29/1457736a186326a.jpg',
      'username': 'nacy@cmu.ac.th',
      'password': 'nr1234',
      'role': 'Teacher'
    }
  ];
  getTeachers(): Observable<Teacher[]> {
    return of(this.teacher);
  }
  getTeacher(id: number): Observable<Teacher> {
    return of(this.teacher)
      .pipe(map(teachers => {
        const output: Teacher = (teachers as Teacher[]).find(teacher => teacher.id === +id);
        return output;
      }));
  }
  getTeacherEmailAndPassword(username: string, password: string): Observable<Teacher> {
    const output: Teacher = this.teacher.find(teacher => teacher.username === username && teacher.password === password);
    return of(this.teacher[output.id - 1 ]);
  }


}
