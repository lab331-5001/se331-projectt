import Activity from '../entity/activity';
import { Observable } from 'rxjs';

export abstract class ActivityService {
    abstract getActivitys(): Observable<Activity[]>;
    abstract getActivity(id: number): Observable<Activity>;
    abstract saveActivity(activity: Activity): Observable<Activity>;
    abstract saveEnrollActivity(id: number);
    abstract check(id: number): boolean;
    abstract getEnrollActivity(): Observable<Activity[]>;
    abstract saveEdit(activity: Activity): Observable<Activity>;
    abstract saveUpdateActivity(id: number);
    abstract deleteActivity(id: number): Observable<Activity>;
}
