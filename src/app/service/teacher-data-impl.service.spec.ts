import { TestBed } from '@angular/core/testing';

import { TeacherDataImplService } from './teacher-data-impl.service';

describe('TeacherDataImplService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TeacherDataImplService = TestBed.get(TeacherDataImplService);
    expect(service).toBeTruthy();
  });
});
