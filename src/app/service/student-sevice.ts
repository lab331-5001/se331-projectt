import { Observable } from 'rxjs';
import Student from '../entity/student';

export abstract class StudentService {
  abstract getStudent(id: number): Observable<Student>;
  abstract getStudents(): Observable<Student[]>;
  abstract saveStudent(student: Student): Observable<Student>;
  abstract getEnrollStudents(): Observable<Student[]>;
  abstract saveEnrollStudent(enrollStudent: Student): Observable<Student>;
  abstract getCheckStudentEmail(username: string): boolean;
  abstract getStudentEmailAndPassword(username: string, password: string): Observable<Student>;
  abstract deleteStudent(id: number): Observable<Student>;
}
