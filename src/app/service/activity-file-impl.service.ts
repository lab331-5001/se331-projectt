import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ActivityService } from './activity-service';
import { map } from 'rxjs/internal/operators/map';
import Activity from '../entity/activity';

@Injectable({
  providedIn: 'root'
})
export class ActivityFileImplService extends ActivityService {
  saveEdit(activity: Activity): Observable<Activity> {
    throw new Error("Method not implemented.");
  }
  saveActivity(activity: Activity): Observable<Activity> {
    activity.id = this.activitys.length + 1;
    this.activitys.push(activity);
    console.log(this.activitys);
    return of(activity);
  }

  // saveUpdateActivity(activity: Activity): Observable<Activity> {
  //   activity.id = this.activitys.length + 1;
  //   this.activitys.push(activity);
  //   console.log(this.activitys);
  //   return of(activity);
  // }

  constructor(private http: HttpClient) {
    super();
  }
  getActivitys(): Observable<Activity[]> {
    return of(this.activitys);
  }
  getActivity(id: number): Observable<Activity> {
    return of(this.activitys)
      .pipe(map(activitys => {
        const output: Activity = (activitys as Activity[]).find(activity => activity.id === +id);
        return output;
      }));
  }
  saveEnrollActivity(id:number) {
    this.enroll.push(this.activitys[id-1]);
    console.log(this.enroll);
  }
// add to update ac page
  saveUpdateActivity(id: number) {
    this.update.push(this.activitys[id-1]);
    console.log(this.update);
  }

  getEnrollActivity():Observable<Activity[]>{
    return of(this.enroll);
  }
  check(id: number): boolean{
    if (this.enroll.find(activity => activity.id == id)){
      return false;
    }else{
      return true;
    }
  }
  deleteActivity(id: number): Observable<Activity> {
    this.enroll.pop();
    return of(this.activitys[id-1]);
  }
  update: Activity[] = [];
  enroll: Activity[] = [];
  activitys: Activity[] =
  [
    {
          "id": 1,
          "activityId": "AC-001",
          "name": 'Activity1',
          'location': "Camt_113",
          "date": "Wednesday",
          'time_start': '13.00',
          'time_end': '14.30',
          'teacher_name': 'Prasert',
          'teacher_surname': 'Somboon',
          'period_reg': '20/10/2018',
          'description': 'Learn every Wednesday',
          'comment': ''
        },
         {
            'id': 2,
          'activityId': 'AC-002',
          'name': 'Camping',
          'location': 'Camt_114',
          'date': 'Wednesday',
          'time_start': '14.30',
          'time_end': '15.00',
          'teacher_name': 'Somjui',
          'teacher_surname': 'Roongroj',
          'period_reg': '20/10/2018',
          'description': 'Learn every Wednesday',
          'comment': ''
        },
        {
          'id': 3,
        'activityId': 'AC-003',
        'name': 'Activity2',
        'location': 'Camt_114',
        'date': 'Wednesday',
        'time_start': '14.30',
        'time_end': '15.00',
        'teacher_name': 'Nacy',
        'teacher_surname': 'Rambo',
        'period_reg': '20/10/2018',
        'description': 'on time',
        'comment':''
      }
    ]


}
