import { Injectable } from '@angular/core';
import { StudentService } from './student-sevice';
import Student from '../entity/student';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StudentFileImplService extends StudentService {
  edit: Student[] = [];
  enrollStudent: Student[] = [];
  student: Student[] = [{
    'id': 1,
    'studentId': 'Stu-01',
    'name': 'Ying',
    'surname': 'Somying',
    'image': 'http://icons-for-free.com/free-icons/png/512/2754576.png',
    'birth': '24/10/1997',
    'username': 'ying@cmu.ac.th',
    'password': '12345'
  }, {
    'id': 2,
    'studentId': 'Stu-02',
    'name': 'Chai',
    'surname': 'Somchai',
    'image': 'https://www.cbbn.nl/wp-content/uploads/2018/04/if_10_avatar_2754575-1.png',
    'birth': '24/01/1997',
    'username': 'chai@cmu.ac.th',
    'password': '54321'
  }];
  constructor(private http: HttpClient) {
    super();
  }

  getStudentEmailAndPassword(username: string, password: string): Observable<Student> {
    const output: Student = this.student.find(student => student.username === username && student.password === password);
    return of(this.student[output.id - 1 ]);
  }
  getStudents(): Observable<Student[]> {
    return this.http.get<Student[]>(environment.studentApi);
  }

  getStudent(id: number): Observable<Student> {
    return this.http.get<Student>(environment.studentApi + '/' + id);
  }

  saveStudent(student: Student): Observable<Student> {
    return this.http.post<Student>(environment.studentApi, student);
  }

  // saveStudent(student: Student): Observable<Student> {
  //   student.id =  this.student.length + 1;
  //   this.student.push(student);
  //   console.log(this.student);
  //   return of(student);
  // }

  getEnrollStudents(): Observable<Student[]> {
    return of(this.enrollStudent);
  }
  saveEnrollStudent(enrollStudent: Student): Observable<Student> {
    enrollStudent.id = this.student.length + 1;
    this.enrollStudent.push(enrollStudent);
    console.log(this.student);
    return of(enrollStudent);
  }
  getCheckStudentEmail(username: string): boolean {
    const output: Student = this.student.find(student => student.username === username);
    if ( output != null ) {
      return true;
    } else {
      return false;
    }
  }
  saveEditActivity(id: number) {
    this.edit.push(this.student[id - 1]);
    console.log(this.edit);
  }
  getEditActivity(): Observable<Student[]> {
    return of(this.edit);
  }
  check(id: number): boolean {
    if (this.edit.find(student => student.id === id)) {
      return false;
    } else {
      return true;
    }
  }
  deleteStudent(id: number): Observable<Student> {
    this.enrollStudent.pop();
    return of(this.student[id-1]);
  }
 
}
