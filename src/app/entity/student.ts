export default class Student {
  id: number;
  studentId: string;
  name: string;
  surname: string;
  image: string;
  birth: string;
  username: string;
  password: string;
}
