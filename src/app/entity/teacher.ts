export class Teacher {
  id: number;
  teacherId: string;
  name: string;
  surname: string;
  subject: string;
  image: string;
  username: string;
  password: string;
  role: string;
}
