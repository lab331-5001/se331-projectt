export default class Activity {
    id: number;
    activityId: string;
    name: string;
    location: string;
    description: string;
    period_reg: string;
    date: string;
    time_start: string;
    time_end: string;
    teacher_name: string;
    teacher_surname: string;
    comment:string;
    image:string;
  }
