import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { TeacherComponent } from './teacher/teacher.component';
import { AdminComponent } from './admin/admin.component';
import { StudentComponent } from './student/student.component';
import { ActivityTableComponent } from './admin/activity-table/activity-table.component';
import { CheckStudentTableComponent } from './admin/checkStudent-table/check.student-table.component';
import { AdminRegisterActivityComponent } from './admin/registerActivity/regisActivity.component';
import { ActivityViewComponent } from './admin/View/activity.view.component';
import { StudentEnrollComponent } from './student/student-enroll/student-enroll.component';
import { ListEnrollComponent } from './student/list-enroll/list-enroll.component';
import { StudentGuard } from './guards/student.guard';
import { LecturerGuard } from './guards/lecturer.guard';
import { AdminGuard } from './guards/admin.guard';
import { ViewStudentComponent } from './student/view-student/view-student.component';
import { EditActivityComponent } from './teacher/edit-activity/edit-activity.component';
import { EditProfileComponent } from './student/edit-profile/edit-profile.component';
import { CommentActivityComponent } from './admin/CommentActivity/commentActivity.component';

const systemRoutes: Routes = [
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'teacher', component: TeacherComponent , canActivate: [LecturerGuard]},
  { path: 'admin', component: AdminComponent , canActivate: [AdminGuard]},
  {path : 'home', component: StudentComponent , canActivate: [StudentGuard] },
  { path: 'listActivity', component: ActivityTableComponent},
  {path: 'registerActivity', component: AdminRegisterActivityComponent},
  {path: 'detail/:id', component: ActivityViewComponent},
  {path: 'checkStudent', component: CheckStudentTableComponent},
  // { path: 'home/:id', component: StudentComponent}
  { path: 'enrollAct', component: StudentEnrollComponent},
  { path: 'listEnroll', component: ListEnrollComponent},
  { path: 'viewStudent', component: ViewStudentComponent},
  //{ path: 'editStudent', component: EditActivityComponent}
  { path: 'editStudent', component: EditProfileComponent},
  { path: 'comment/:id', component:CommentActivityComponent}
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(systemRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class SystemRoutingModule {
}
