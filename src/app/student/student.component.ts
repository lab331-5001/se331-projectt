import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router , Params} from '@angular/router';
import { StudentService } from '../service/student-sevice';
import Student from '../entity/student';
import { StudentFileImplService } from '../service/student-file-impl.service';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {
student: Student = {
    'id': 2,
    'studentId': 'Stu-02',
    'name': 'Chai',
    'surname': 'Somchai',
    'image': 'https://www.cbbn.nl/wp-content/uploads/2018/04/if_10_avatar_2754575-1.png',
    'birth': '24/01/1997',
    'username': 'chai@cmu.ac.th',
    'password': '54321'
  };

  ngOnInit(): void {
    console.log(localStorage);
    console.log(this.studentService.getStudent(2));


    this.studentService.getStudent(parseInt(localStorage.getItem('studentId'))).subscribe(student => {
      this.student = student;
    });
    // this.route.params.subscribe((params: Params) => {
    //   this.studentService.getStudent(+params['id']).subscribe((inputStudent: Student) => this.student = inputStudent);
  }

  constructor(private route: ActivatedRoute, private studentService: StudentFileImplService, private router: Router ) { }
}
