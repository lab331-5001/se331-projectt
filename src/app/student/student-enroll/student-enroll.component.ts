import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import Activity from '../../entity/activity';
import { Observable, of as observableOf } from 'rxjs';
import { ActivityService } from '../../service/activity-service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ViewChild } from '@angular/core';
import { StudentEnrollDataSource } from './student-enroll-datasource';
import { Router } from '@angular/router';


@Component({
  selector: 'app-student-enroll',
  templateUrl: './student-enroll.component.html',
  styleUrls: ['./student-enroll.component.css']
})
export class StudentEnrollComponent implements OnInit {
  model: Activity  = new Activity();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: StudentEnrollDataSource ;
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'activityId', 'name', 'location', 'date', 'time_start', 'time_end', 'teacher_name','teacher_surname', 'period_reg', 'description', 'enroll'];
  activitys: Activity[];
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor(private activityService: ActivityService, private router:Router) { }
  ngOnInit() {
    this.activityService.getActivitys()
      .subscribe(activitys => {
        this.dataSource = new StudentEnrollDataSource(this.paginator,this.sort);
        this.dataSource.data = activitys;
        this.activitys = activitys;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
      }
      );
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
    this.filter$.next(filterValue.trim().toLowerCase());
  }
  enroll(activity: any) {
    if (this.activityService.check(activity.id)){
      this.activityService.saveEnrollActivity(activity.id);
      this.router.navigate(['/listEnroll',activity.id]);
    }else{
      alert('Can not enroll');
    }
}
get diagnostic() { return JSON.stringify(this.model);}

 }


