import { Component, OnInit } from '@angular/core';
import Student from '../../entity/student';
import { ActivatedRoute } from '@angular/router';
import { StudentService } from '../../service/student-sevice';
import { Router } from '@angular/router';
import { Params } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
  student: Student;
  model: Student = new Student();
  fb: FormBuilder = new FormBuilder();
  sf: FormGroup;
  constructor(private route: ActivatedRoute, private studentService: StudentService, private router: Router) { }
  ngOnInit() {
    this.route.params
      .subscribe((params: Params) => {
        this.studentService.getStudent(+params['id'])
          .subscribe((inputStudent: Student) => this.student = inputStudent);
      });
  }
  onSubmit(){
    this.model = this.sf.value;
  this.studentService.saveStudent(this.model)
  .subscribe((activity) => {
      this.router.navigate(['/home']);
    }, (error) => {
      alert('could not save value');
    });
}

}
