import { Component, OnInit } from '@angular/core';
import { StudentService } from '../../service/student-sevice';
import Student from '../../entity/student';
import { ActivatedRoute } from '@angular/router';
import { Params } from '@angular/router';

@Component({
  selector: 'app-view-student',
  templateUrl: './view-student.component.html',
  styleUrls: ['./view-student.component.css']
})
export class ViewStudentComponent implements OnInit {

  student: Student;
  constructor(private route: ActivatedRoute, private studentService: StudentService) { }
  ngOnInit() {
    this.route.params
      .subscribe((params: Params) => {
        this.studentService.getStudent(+params['id'])
          .subscribe((inputStudent: Student) => this.student = inputStudent);
      });
  }

}
