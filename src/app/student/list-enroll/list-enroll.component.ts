import { Component, OnInit } from '@angular/core';
import { ListEnrollDataSource } from './list-enroll-datasource';
import { BehaviorSubject } from 'rxjs';
import { ActivityService } from '../../service/activity-service';
import Activity from '../../entity/activity';
import { MatSort, MatPaginator } from '@angular/material';
import { ViewChild } from '@angular/core';

@Component({
  selector: 'app-list-enroll',
  templateUrl: './list-enroll.component.html',
  styleUrls: ['./list-enroll.component.css']
})
export class ListEnrollComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: ListEnrollDataSource;
  displayedColumns = ['id', 'activityId', 'name', 'location', 'date', 'time_start', 'time_end', 'teacher_name','teacher_surname', 'period_reg', 'description'];
  activitys: Activity[];
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor(private activityService: ActivityService) { }
  ngOnInit() {
    this.activityService.getEnrollActivity()
      .subscribe(activitys => {
        this.dataSource = new ListEnrollDataSource(this.paginator, this.sort);
        this.dataSource.data = activitys;
        this.activitys = activitys;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
      }
      );
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
    this.filter$.next(filterValue.trim().toLowerCase());
  }

}