import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListEnrollComponent } from './list-enroll.component';

describe('ListEnrollComponent', () => {
  let component: ListEnrollComponent;
  let fixture: ComponentFixture<ListEnrollComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListEnrollComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListEnrollComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
