import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { error } from '@angular/compiler/src/util';
import Activity from '../../entity/activity';
import { NavServiceService } from '../../nav-service.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivityService } from '../../service/activity-service';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-admin-registerActivity',
  templateUrl: './regisActivity.component.html',
  styleUrls: ['./regisActivity.component.css']
})
export class AdminRegisterActivityComponent {
  model: Activity  = new Activity();
//TODO: Remove this when we're done

constructor(private activityService: ActivityService, private router: Router){}


onSubmit(){
  this.activityService.saveActivity(this.model)
  .subscribe((activity) => {
      this.router.navigate(['/listActivity']);
    }, (error) => {
      alert('could not save value');
    });
}
  get diagnostic() { return JSON.stringify(this.model);}

 }
