
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { CheckStudentTableComponent } from './check.student-table.component';



describe('CheckStudentTableComponent', () => {
  let component: CheckStudentTableComponent;
  let fixture: ComponentFixture<CheckStudentTableComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckStudentTableComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CheckStudentTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
