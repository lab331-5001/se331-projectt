import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Observable, of as observableOf, BehaviorSubject } from 'rxjs';
import { CheckStudentTableDataSource } from './check.student-table-datasource';
import { StudentService } from '../../service/student-sevice';
import Student from '../../entity/student';

@Component({
  selector: 'app-CheckStudent-table',
  templateUrl: './check.student-table.component.html',
  styleUrls: ['./check.student-table.component.css']
})
export class CheckStudentTableComponent implements OnInit {
    router: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: CheckStudentTableDataSource;
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'studentId', 'name', 'surname', 'image', 'accept', 'reject'];
  students: Student[];
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor(private studentService: StudentService) { }
  ngOnInit() {
    this.studentService.getStudents()
      .subscribe(students => {
        this.dataSource = new CheckStudentTableDataSource(this.paginator, this.sort);
        this.dataSource.data = students;
        this.students = students;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
      }
      );
  }
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

  accept(student: any) {
      alert('You are accepted');
      this.studentService.saveStudent(student);
      this.studentService.deleteStudent(student);
      this.router.navigate(['/homeadmin']);

  }

  reject(student: any) {
      alert('You are rejected');
      this.studentService.deleteStudent(student);
      this.router.navigate(['/homeadmin']);
  }
}
