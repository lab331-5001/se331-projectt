import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import Activity from '../../entity/activity';
import { ActivityService } from '../../service/activity-service';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import activity from '../../entity/activity';
import { environment } from '../../../environments/environment.prod';


@Component({
  selector: 'app-admin-CommentActivity',
  templateUrl: './commentActivity.html',
  styleUrls: ['./commentActivity.css']
})
export class CommentActivityComponent implements OnInit {
  activity: Activity;
  model: Activity = new Activity();
  fb: FormBuilder = new FormBuilder();
  sf: FormGroup;
  constructor(private route: ActivatedRoute, private activityService: ActivityService, private router: Router) { }
  // uploadEndPoint: string;
  // uploadedUrl: string;
  // processReturnFile(file: File, event: any):void{
  //   if(event.event.type === 'load'){
  //     console.log(event.event.target.response);
  //     this.uploadedUrl = event.event.target.response;
  //   }
  // }
  ngOnInit(): void {
    // this.uploadEndPoint = environment.uploadApi;
    this.route.params
      .subscribe((params: Params) => {

        this.activityService.getActivity(+params['id'])
          .subscribe((inputActivity: Activity) => this.activity = inputActivity);
      });
  }
  onSubmit() {
    this.model = this.sf.value;
    // this.model.image = this.uploadedUrl;
    this.activityService.saveActivity(this.model)
      .subscribe((student) => {
        this.router.navigate(['/detail/:id']);
      }, (error) => {
        alert('could not save value');
      });
  }
}
