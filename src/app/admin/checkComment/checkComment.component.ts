import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import Activity from '../../entity/activity';
import { ActivityService } from '../../service/activity-service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-admin-checkComment',
  templateUrl: './checkComment.component.html',
  styleUrls: ['./checkComment.component.css']
})
export class checkCommentComponent implements OnInit {
  model: Activity  = new Activity();
  activity: Activity;
  constructor(private route: ActivatedRoute, private activityService: ActivityService, private router: Router) { }
  ngOnInit() {
    this.route.params
      .subscribe((params: Params) => {
        this.activityService.getActivity(+params['id'])
          .subscribe((inputActivity: Activity) => this.activity = inputActivity);
      });
  }
  onSubmit(activity: any){
    alert("You are rejected");
    this.activityService.deleteActivity(activity);
    this.router.navigate(['/checkCommnent',activity.id]);
  }
  
}
