import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ActivityTableDataSource } from './activity-table-datasource';
import Activity from '../../entity/activity';
import { Observable, of as observableOf, BehaviorSubject } from 'rxjs';
import { ActivityService } from '../../service/activity-service';

@Component({
 selector: 'app-activity-table',
  templateUrl: './activity-table.component.html',
  styleUrls: ['./activity-table.component.css']
})
export class ActivityTableComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: ActivityTableDataSource;
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'activityId', 'name', 'location', 'date', 'time_start', 'time_end', 'teacher_name','teacher_surname', 'period_reg', 'description'];
  activitys: Activity[];
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor(private activityService: ActivityService) { }
  ngOnInit() {
    this.activityService.getActivitys()
      .subscribe(activitys => {
        this.dataSource = new ActivityTableDataSource(this.paginator, this.sort);
        this.dataSource.data = activitys;
        this.activitys = activitys;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
      }
      );
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
    this.filter$.next(filterValue.trim().toLowerCase());
  }

}
