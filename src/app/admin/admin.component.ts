import { Component, OnInit } from '@angular/core';
import { Admin } from '../entity/admin';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
model: Admin =  new Admin();
password: string;
  constructor(private router: Router) { }

  ngOnInit() {
  }

onSubmit() {
  if ((this.model.password) === '12345') {
    alert('hello admin');
    localStorage.setItem('isLoginAdmin', 'true');
    this.router.navigate(['/homeadmin']);
  } else {
    alert('Please check your password');
  }
}
}
