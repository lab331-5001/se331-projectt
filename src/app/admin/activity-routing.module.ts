import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AdminRegisterActivityComponent } from './registerActivity/regisActivity.component';
import { ActivityTableComponent } from './activity-table/activity-table.component';
import { ActivityViewComponent } from './View/activity.view.component';
import { HomeAdminComponent } from './homeAdmin/admin.home.component';
import { CheckStudentTableComponent } from './checkStudent-table/check.student-table.component';
import { checkCommentComponent } from './checkComment/checkComment.component';


const ActivityRoutes: Routes = [
    { path: 'listActivity', component: ActivityTableComponent },
    { path: 'registerActivity', component: AdminRegisterActivityComponent },
    { path: 'detail/:id', component: ActivityViewComponent },
    { path: 'homeadmin', component: HomeAdminComponent },
    { path: 'checkStudent', component: CheckStudentTableComponent},
    { path: 'checkComment/:id', component: checkCommentComponent}
    //{ path: 'checkStudent', component: CheckStudentTableComponent}
];
@NgModule({
    imports: [
        RouterModule.forRoot(ActivityRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class ActivityRoutingModule {

}
