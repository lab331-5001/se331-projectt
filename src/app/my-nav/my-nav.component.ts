import { Component, OnInit, DoCheck } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { NavServiceService } from '../nav-service.service';
import { StudentService } from '../service/student-sevice';
import { AuthenticationService } from '../service/authentication.service';
import Student from '../entity/student';

@Component({
  selector: 'app-my-nav',
  templateUrl: './my-nav.component.html',
  styleUrls: ['./my-nav.component.css'],
})
export class MyNavComponent implements OnInit, DoCheck {
  defaultImageUrl = 'http://18.236.187.57:8091/images/admin.png';
  student: Student;
  isFirstpage = localStorage.getItem('isFirstpage') === 'false';
  isLogin = localStorage.getItem('isLogin') === 'true';
  isLoginAdmin = localStorage.getItem('isLoginAdmin') === 'true';

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );
    students$: Observable<Student[]> = this.studentService.getStudents();
    student$: Observable<Student> = this.studentService.getStudent(1);

  ngOnInit(): void {
    localStorage.setItem('isLoginAdmin', 'false');
  }
  ngDoCheck(): void {
    this.isLogin = localStorage.getItem('isLogin') === 'true';
    this.isLoginAdmin = localStorage.getItem('isLoginAdmin') === 'true';
  }
  logoutAdmin() {
    localStorage.setItem('isLoginAdmin', 'false');
  }
  // constructor(private breakpointObserver: BreakpointObserver, private nav: NavServiceService) {}
  constructor(private breakpointObserver: BreakpointObserver, private studentService: StudentService
    , private authService: AuthenticationService) { }

  hasRole(role: string) {
    return this.authService.hasRole(role);
  }

  get user() {
    return this.authService.getCurrentUser();
  }
}

