import { Component, OnInit } from '@angular/core';
import Student from '../entity/student';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { StudentService } from '../service/student-sevice';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
student: Student;
model: Student = new Student();
fb: FormBuilder = new FormBuilder();
sf: FormGroup;

validation_messages = {
  'studentId': [
    { type: 'required', message: 'Student Id is required'},
    { type: 'pattern', message: 'Please enter your studentId (ex. Stu-xx)'}
  ],
  'name': [
    {type: 'required', message: 'Please enter your name'}
  ],
  'surname': [
    { type: 'required', message: 'Please enter your surname'}
  ],
  'image': [],
  'birth': [
    { type: 'required', message: 'Please enter your birthday'},
    { type: 'pattern', message: 'Birthday should be dd/mm/yyyy'}
  ],
  'username': [
    { type: 'required', message: 'Please enter your email'},
    { type: 'pattern', message: 'Email should be xxx@cmu.ac.th'}
  ],
  'password': [
    { type: 'required', message: 'Please enter your password'}
  ]
};
  constructor(private studentService: StudentService, private router: Router) {}
  ngOnInit(): void {
    this.sf = this.fb.group({
      studentId: [null, Validators.compose([Validators.required,
      Validators.pattern('^(Stu-)[0-9]+')])],
      name: [null, Validators.required],
      surname: [null, Validators.required],
      image: [null],
      birth: [null, Validators.compose([Validators.required])],
      // Validators.pattern('^(0[1-9]|1[012])[-/.](0[1-9]|[12][0-9]|3[01])[-/.](19|20)\\d\\d$')]
      username: [null, Validators.compose([Validators.required, Validators.pattern('')])],
      // [^[a-z0-9._%+-]+@(cmu.ac.th)
      password: [null, Validators.compose([Validators.required])],
    });
  }
onSubmitRegisStudent() {
  this.model = this.sf.value;
  if (!this.studentService.getCheckStudentEmail(this.model.username)) {
    this.studentService.saveEnrollStudent(this.model)
    .subscribe((student) => {
      this.router.navigate(['/login']);
      alert('Register is successful');
    }, (error) => {
      alert('Could not save your registration');
    });
  } else {
    alert('This email is already used');
  }
}
}
