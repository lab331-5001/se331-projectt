import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyNavComponent } from './my-nav/my-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule,
  MatFormFieldModule, NativeDateModule, MAT_DATE_LOCALE } from '@angular/material';
import {  MatGridListModule, MatCardModule,
    MatMenuModule, MatTableModule, MatPaginatorModule, MatSortModule, MatDatepickerModule, MatDatepickerToggle,
      MatFormFieldControl,
      MatInputModule, MatProgressSpinnerModule} from '@angular/material';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { TeacherComponent } from './teacher/teacher.component';
import { AdminComponent } from './admin/admin.component';
import { AppRoutingModule } from './app-routing.module';
import { SystemRoutingModule } from './system-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { StudentComponent } from './student/student.component';
import { StudentFileImplService } from './service/student-file-impl.service';
import { StudentService } from './service/student-sevice';
import { TeacherService } from './service/teacher-service';
import { TeacherDataImplService } from './service/teacher-data-impl.service';
import { CheckStudentTableComponent } from './admin/checkStudent-table/check.student-table.component';
import { ActivityComponent } from './admin/list/activity.component';
import { ActivityViewComponent } from './admin/View/activity.view.component';
import { AdminRegisterActivityComponent } from './admin/registerActivity/regisActivity.component';
import { ActivityTableComponent } from './admin/activity-table/activity-table.component';
import { ActivityRoutingModule } from './admin/activity-routing.module';
import { ActivityService } from './service/activity-service';
import { ActivityFileImplService } from './service/activity-file-impl.service';
import { HomeAdminComponent } from './admin/homeAdmin/admin.home.component';
import { ViewComponent } from './teacher/view/view.component';
import { TeacherRoutingModule } from './teacher/teacher-routing.module';
import { AclistTableComponent } from './teacher/aclist-table/aclist-table.component';
import { NavServiceService } from './nav-service.service';
import { StudentEnrollComponent } from './student/student-enroll/student-enroll.component';
import { ListEnrollComponent } from './student/list-enroll/list-enroll.component';
import { EditProfileComponent } from './student/edit-profile/edit-profile.component';
import { ActivityUpdateComponent } from './teacher/activity-update/activity-update.component';
import { EditActivityComponent } from './teacher/edit-activity/edit-activity.component';
import { CheckRegStdTableComponent } from './teacher/check-reg-std-table/check-reg-std-table.component';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';
import { ViewStudentComponent } from './student/view-student/view-student.component';
import { CommentActivityComponent } from './admin/CommentActivity/commentActivity.component';
import { MatFileUploadModule } from 'angular-material-fileupload'; 
import { checkCommentComponent } from './admin/checkComment/checkComment.component';
import { SeeActivityTableComponent } from './teacher/see-activity-table/see-activity-table.component';


@NgModule({
  declarations: [
    AppComponent,
    MyNavComponent,
    LoginComponent,
    RegisterComponent,
    TeacherComponent,
    AdminComponent,
    StudentComponent,
    ActivityTableComponent,
    AdminRegisterActivityComponent,
    ActivityViewComponent,
    ActivityComponent,
    CheckStudentTableComponent,
    HomeAdminComponent,
    StudentComponent,
    ViewComponent,
    AclistTableComponent,
    ActivityUpdateComponent,
    EditActivityComponent,
    CheckRegStdTableComponent,
    StudentEnrollComponent,
    ListEnrollComponent,
    EditProfileComponent,
    FileNotFoundComponent,
    ViewStudentComponent,
    CommentActivityComponent,
    checkCommentComponent,
    SeeActivityTableComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatFormFieldModule,
    SystemRoutingModule,
    TeacherRoutingModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    ActivityRoutingModule,
    MatProgressSpinnerModule,
    AppRoutingModule,
    // MatToolbarModule,
    // MatButtonModule,
    // MatSidenavModule,
    // MatIconModule,
    // MatListModule
    MatFileUploadModule 

  ],
  providers: [
    { provide: StudentService, useClass: StudentFileImplService},
    { provide: TeacherService, useClass: TeacherDataImplService },
    { provide: ActivityService, useClass: ActivityFileImplService }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
