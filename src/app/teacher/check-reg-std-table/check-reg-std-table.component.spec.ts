import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckRegStdTableComponent } from './check-reg-std-table.component';

describe('CheckRegStdTableComponent', () => {
  let component: CheckRegStdTableComponent;
  let fixture: ComponentFixture<CheckRegStdTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckRegStdTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckRegStdTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
