import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Observable, of as observableOf, BehaviorSubject } from 'rxjs';

import { StudentService } from '../../service/student-sevice';
import Student from '../../entity/student';
import { CheckStudentTableDataSource } from 'src/app/admin/checkStudent-table/check.student-table-datasource';
import Activity from 'src/app/entity/activity';
import { Router } from '@angular/router';

@Component({
  selector: 'app-check-reg-std-table',
  templateUrl: './check-reg-std-table.component.html',
  styleUrls: ['./check-reg-std-table.component.css']
})
export class CheckRegStdTableComponent implements OnInit {
 // router: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: CheckStudentTableDataSource;
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
   displayedColumns = ['id', 'studentId', 'name', 'surname', 'image', 'activityId', 'accept', 'reject'];
  // displayedColumns = ['id', 'studentId', 'name', 'surname', 'image', 'accept', 'reject'];
  students: Student[];
  activity: Activity[];
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor(private studentService: StudentService, private router: Router) { }
  ngOnInit() {
    this.studentService.getEnrollStudents()
      .subscribe(enrollStudent => {
        this.dataSource = new CheckStudentTableDataSource(this.paginator, this.sort);
        this.dataSource.data = enrollStudent;
        this.students = enrollStudent;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
      }
      );
  }
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

  accept(student: any) {
      alert('You are accepted');
      this.studentService.saveStudent(student);
      this.studentService.deleteStudent(student);
      this.router.navigate(['/login']);  // ถ้าlogin teacher ได้ละ แก้เป็นพาร์ท hometeacher
  }

  reject(student: any) {
      alert('You are rejected');
      this.studentService.deleteStudent(student);
      this.router.navigate(['/login']);
  }
}
