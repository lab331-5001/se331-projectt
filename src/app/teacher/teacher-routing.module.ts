import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewComponent } from './view/view.component';
import { AclistTableComponent } from './aclist-table/aclist-table.component';
import { ActivityUpdateComponent } from './activity-update/activity-update.component';
import { EditActivityComponent } from './edit-activity/edit-activity.component';
import { CheckRegStdTableComponent } from './check-reg-std-table/check-reg-std-table.component';
import { SeeActivityTableComponent } from './see-activity-table/see-activity-table.component';

const TeacherRoutes: Routes = [

  { path: 'hometeacher/:id', component: ViewComponent },
  { path: 'teacherUpdateAc', component: AclistTableComponent },
  { path: 'detailNew/:id', component: EditActivityComponent },
  { path: 'studentlist', component: CheckRegStdTableComponent },
  { path: 'seeActivityList', component: SeeActivityTableComponent },
];
@NgModule ({
  imports: [
    RouterModule.forRoot(TeacherRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class TeacherRoutingModule {
}

