import { Component, OnInit } from '@angular/core';
import { Teacher } from '../entity/teacher';
import { TeacherService } from '../service/teacher-service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.css']
})
export class TeacherComponent implements OnInit {
teacher: Teacher;
modelTeacher: Teacher = new Teacher();

  constructor(private teacherService: TeacherService, private router: Router) { }

  ngOnInit() {
  }
loginTeacher() {
  this.teacherService.getTeacherEmailAndPassword(this.modelTeacher.username, this.modelTeacher.password)
  .subscribe((teacher) => {
    if (teacher.id != null ) {
      this.router.navigate(['/hometeacher']);
    }
  });
}
}
