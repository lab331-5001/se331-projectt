import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { BehaviorSubject } from 'rxjs';
import { AclistTableDataSource } from './aclist-table-datasource';
import Activity from 'src/app/entity/activity';
import { ActivityService } from 'src/app/service/activity-service';
import { Router } from '@angular/router';
import { NavServiceService } from '../../nav-service.service';

@Component({
  selector: 'app-aclist-table',
  templateUrl: './aclist-table.component.html',
  styleUrls: ['./aclist-table.component.css']
})

export class AclistTableComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: AclistTableDataSource;
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'activityId', 'name', 'location', 'period_reg', 'date',
    'time_start', 'time_end', 'teacher_name', 'teacher_surname', 'description', 'update'];
  activitys: Activity[];
  filter: string;
  filter$: BehaviorSubject<string>;

  constructor(private activityService: ActivityService, private router: Router, private nav: NavServiceService ) { }
  ngOnInit() {
    this.activityService.getActivitys()
      .subscribe(activitys => {
        this.dataSource = new AclistTableDataSource(this.paginator, this.sort);
        this.dataSource.data = activitys;
        this.activitys = activitys;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
      }
      );
      if (this.nav.visible) { // to show activity of each teacher
        this.applyFilter(this.nav.name);
      }

  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLocaleLowerCase();
    this.dataSource.filter = filterValue;
    this.filter$.next(filterValue.trim().toLowerCase());
  }

  update(activity: any) {
    this.activityService.getActivity(activity.id)
    // tslint:disable-next-line:no-shadowed-variable
    .subscribe((activity) => {
      this.router.navigate(['detailNew', activity.id]);
    }, (error) => {
    alert('Could not save value');
    });
  }
  // add(activity: any) {
  //   if (this.activityService.check(activity.id)) {
  //     this.activityService.saveEnroll(activity.id);
  //     this.router.navigate(['/']);
  //   } else {
  //     alert("Error");
  //   }
  // }


}
