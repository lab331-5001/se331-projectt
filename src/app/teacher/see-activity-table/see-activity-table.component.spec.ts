import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeeActivityTableComponent } from './see-activity-table.component';

describe('SeeActivityTableComponent', () => {
  let component: SeeActivityTableComponent;
  let fixture: ComponentFixture<SeeActivityTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeeActivityTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeeActivityTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
