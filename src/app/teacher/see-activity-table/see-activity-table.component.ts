import { Component, OnInit, ViewChild } from '@angular/core';
import { SeeActivityTableDataSource } from './see-activity-table-datasource';
import { MatPaginator, MatSort } from '@angular/material';
import Activity from 'src/app/entity/activity';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { ActivityService } from 'src/app/service/activity-service';
import { NavServiceService } from 'src/app/nav-service.service';

@Component({
  selector: 'app-see-activity-table',
  templateUrl: './see-activity-table.component.html',
  styleUrls: ['./see-activity-table.component.css']
})
export class SeeActivityTableComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: SeeActivityTableDataSource;

  displayedColumns = ['id', 'activityId', 'name', 'location', 'period_reg', 'date',
  'time_start', 'time_end', 'teacher_name', 'teacher_surname', 'description'];
  activitys: Activity[];
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor(private activityService: ActivityService, private router: Router, private nav: NavServiceService ) { }

  ngOnInit() {
    this.activityService.getActivitys()
    .subscribe(activitys => {
      this.dataSource = new SeeActivityTableDataSource(this.paginator, this.sort);
      this.dataSource.data = activitys;
      this.activitys = activitys;
      this.filter$ = new BehaviorSubject<string>('');
      this.dataSource.filter$ = this.filter$;
    }
    );
    if (this.nav.visible) { // to show activity of each teacher
      this.applyFilter(this.nav.name);
    }

}
applyFilter(filterValue: string) {
  filterValue = filterValue.trim();
  filterValue = filterValue.toLocaleLowerCase();
  this.dataSource.filter = filterValue;
  this.filter$.next(filterValue.trim().toLowerCase());
}

}
