import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import Activity from '../../entity/activity';
import { ActivityService } from '../../service/activity-service';

@Component ({
  selector: 'app-activity-update',
  templateUrl: './activity-update.component.html',
  styleUrls: ['./activity-update.component.css']
})
export class ActivityUpdateComponent implements OnInit {
    activity: Activity;
    constructor(private route: ActivatedRoute, private activityService: ActivityService) { }
    ngOnInit() {
      this.route.params
        .subscribe((params: Params) => {
          this.activityService.getActivity(+params['id'])
            .subscribe((inputActivity: Activity) => this.activity = inputActivity);
        });
    }
  }
