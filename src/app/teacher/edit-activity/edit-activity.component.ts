import { Component, OnInit } from '@angular/core';
import Activity from 'src/app/entity/activity';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ActivityService } from 'src/app/service/activity-service';

@Component({
  selector: 'app-edit-activity',
  templateUrl: './edit-activity.component.html',
  styleUrls: ['./edit-activity.component.css']
})
export class EditActivityComponent implements OnInit {
  activity: Activity;
  model: any;
  constructor(private route: ActivatedRoute, private activityService: ActivityService, private router: Router) { }
  ngOnInit() {
    this.route.params
      .subscribe((params: Params) => {
        this.activityService.getActivity(+params['id'])
          .subscribe((inputActivity: Activity) => this.activity = inputActivity);
      });
  }

  onSubmit() {
    // this.model = this.af.value;
    this.model.id = this.activity.id;
    this.activityService.saveUpdateActivity(this.model)
    .subscribe((activity) => {
        this.router.navigate(['/teacheractivity']);
      }, (error) => {
        alert('could not save value');
      });
  }

  // Update(activity: any) {
  //   if (this.activityService.check(activity.id)) {
  //     this.activityService.saveUpdateActivity(activity.id);
  //     this.router.navigate(['/teacheractivity']);
  //   } else {
  //     alert('Can not update');
  //   }
  // }

  get diagnostic() {
    return JSON.stringify(this.model);
  }
}
