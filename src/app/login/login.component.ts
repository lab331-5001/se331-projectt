import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import Student from '../entity/student';
import { StudentService } from '../service/student-sevice';
import { NavServiceService } from '../nav-service.service';
import { Validators, FormBuilder } from '@angular/forms';
import { AuthenticationService } from '../service/authentication.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  addressForm = this.fb.group({
    username: [null, Validators.required],
    password: [null, Validators.required]
  });
student: Student;
model: Student = new Student();
  constructor(private studentService: StudentService, private router: Router, private nav: NavServiceService) { }
  ngOnInit() {
    localStorage.setItem('isLogin', 'false');
  }
  // loginStudent() {
  //   this.studentService.getStudentEmailAndPassword(this.model.username, this.model.password)
  //   .subscribe((student) => {
  //     console.log(student);
  //     if (student.id != null) {
  //       this.nav.setStudent(student.id, student.studentId, student.name, student.surname, student.birth, student.image, student.username);
  //       localStorage.setItem('isLogin', 'true');
  //       localStorage.setItem('studentId', JSON.stringify(student.id));
  //       this.router.navigate(['/home']);
  //     } else {
  //       alert(' Please check your username and password');
  //     }
  //   });
  // }
  onSubmit() {
    const value = this.addressForm.value;
    this.authenService.login(value.username, value.password)
      .subscribe(
        data => {
          this.isError = false;
          this.router.navigate([this.returnUrl]);
        },
        error => {
          this.isError = true;
        }
      );
    console.log(this.addressForm.value);
  }
}
